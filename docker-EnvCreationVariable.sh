### HOST VARIABLES
# this is the user used by ERDDAP to access the MYDOCKER_DATA_DIR
ERDDAP_user="usreuskoos"
# this is the password used t oaccess ncWMS admin interface 
ncWMS_PASSWORD="mystrongpassword!"

### DOCKER
# this folder where are created all the deployement directories.
MYDOCKER_ROOT_DIR="/home/opt/euskoos/"
# this is folder where you put data accessible by ERDDAP,ncWMS,API
MYDOCKER_DATA_DIR="/home/opt/euskoos/data"
# this is the port used by the ERDDAP container
MYDOCKER_HOST_ERDDAP_PORT="12081"
# this is the port used by the ncWMS container
MYDOCKER_HOST_ncWMS_PORT="12082"
# this is the port used by the API container
MYDOCKER_HOST_API_PORT="12083"

### APACHE VARIABLES
#These are default paths for Apache on CentOS, change them if different
APACHE_SITE_CONF_DIR="/etc/httpd/conf.d"
APACHE_SERVICE_NAME="httpd"
APACHE_SITE_PATH="/var/www/html"
APACHE_LOG_DIR="/var/log/httpd"
# For the VHOST_FILENAME keep the 00_ if you want that this is the first vhost loaded by Apache
# It will be used as filename for the configuration file
APACHE_VHOST_FILENAME="00_euskoos"
# Put here the IPv4 or the domain (without http/https, i.e: www.somedomain.com)
APACHE_DOMAIN_ROOT="62.94.85.20"
# change this variable if you need to access ERDDAP with a different path than http(s)://{APACHE_DOMAIN_ROOT}/erddap/
APACHE_PROXY_LOCATION_ERDDAP="/erddap"
APACHE_PROXY_LOCATION_ncWMS="/ncWMS"
APACHE_PROXY_LOCATION_API="/API"
# change these variables if the docker container is not accessible at 127.0.0.1
APACHE_PROXY_URL_PROXYPASS_ERDDAP="http://127.0.0.1:${MYDOCKER_HOST_ERDDAP_PORT}/erddap"
APACHE_PROXY_URL_PROXYPASSREVERSE_ERDDAP="http://127.0.0.1:${MYDOCKER_HOST_ERDDAP_PORT}/erddap"

APACHE_PROXY_URL_PROXYPASS_ncWMS="http://127.0.0.1:${MYDOCKER_HOST_ncWMS_PORT}/ncWMS"
APACHE_PROXY_URL_PROXYPASSREVERSE_ncWMS="http://127.0.0.1:${MYDOCKER_HOST_ncWMS_PORT}/ncWMS"

APACHE_PROXY_URL_PROXYPASS_API="http://127.0.0.1:${MYDOCKER_HOST_API_PORT}"
APACHE_PROXY_URL_PROXYPASSREVERSE_API="http://127.0.0.1:${MYDOCKER_HOST_API_PORT}"

#change these for SSL if needed
APACHE_ENABLE_SSL="NO" # set a YES if you want to enable SSL
APACHE_SSLCertificateFile="path to SSL Certificate File"      
APACHE_SSLCertificateKeyFile="path to SSL Certificate Key File"   

### ERDDAP VARIABLES
# Environment Variables - Starting with ERDDAP v2.14, ERDDAP administrators can override any value in setup.xml by specifying an environment variable named ERDDAP_valueName before running ERDDAP
# bigParentDirectory - Same as volume mounted in the container
ERDDAP_bigParentDirectory="/erddapData"
# baseUrl - Same as the URL that you setup in the web proxy that handle the comunication with the container
ERDDAP_baseUrl="http://${APACHE_DOMAIN_ROOT}"
# baseUrl - Same as the URL that you setup in the web proxy that handle the SSL comunication with the container
ERDDAP_baseHttpsUrl="https://${APACHE_DOMAIN_ROOT}"
# CHANGE THE FOLLOWING VARIABLES
ERDDAP_emailEverythingTo="marco.alba@ettsolutions.com"
ERDDAP_adminInstitution="ETT S.p.A. - People and Technology"
ERDDAP_adminInstitutionUrl="https://www.ettsolutions.com/"
ERDDAP_adminIndividualName="ETT Ricerca"
ERDDAP_adminPosition="ERDDAP administrator"
ERDDAP_adminPhone="+39 010 6519116"
ERDDAP_adminAddress="Via Sestri 37"
ERDDAP_adminCity="GENOVA"
ERDDAP_adminStateOrProvince="GE"
ERDDAP_adminPostalCode="16154"
ERDDAP_adminCountry="ITALY"
ERDDAP_adminemail="ricerca.innovazione.ett@ettsolutions.com"
ERDDAP_flagKeyKey="No dark sarcasm in the classroom"
# these variables set the minimum and maximum memory available in ERDDAP, set by default at 4 GigaBytes
ERDDAP_MIN_MEMORY="4G"
ERDDAP_MAX_MEMORY="8G"


### API configuration
API_log_path="/data/log/"
ftp_server="ftp2.azti.es"
ftp_user="dmc"
ftp_password="boyasWS"

### MySQL connection
# set a YES if there is no direct access to MySQL database and a ssh tunnel is needed
createtunnel="YES"
# mysqlserver and mysqlport ignored if createtunnel="YES"
mysqlserver="???"
mysqlport="???"
# sshpass, sshserver and sshport ignored if createtunnel="NO"
sshpass="2020AZTIbbdd"
sshserver="212.142.134.200"
sshport="2218"




