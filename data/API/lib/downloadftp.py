import shutil
import urllib.request as request
from contextlib import closing
import os
import ftplib
from dateutil import parser
from datetime import datetime
import time
import pandas as pd
import re
from lib.utility import fixTimeUnits

class customDownloader():
    def __init__(self, ftp_server,ftp_user,ftp_password,log):
        self.ftp_server=ftp_server
        self.ftp_user=ftp_user
        self.ftp_password=ftp_password
        self.log=log

    def downloadFile(self,remotefile,basepath,localfile):
        try:
            self.log.logger.info("Download "+remotefile)
            with closing(request.urlopen('ftp://'+self.ftp_user+':'+self.ftp_password+'@'+self.ftp_server+'/'+remotefile)) as r:
                with open(os.path.join (basepath,localfile), 'wb') as f:
                    shutil.copyfileobj(r, f)
            return True
        except Exception as exp:
            self.log.logger.error(exp)
            return False
    
    def downloadFiles(self,remotefileDir,regEx,basepath,fixtimeunits=False,nameforlastfile=""):
        try:            
            ftp = ftplib.FTP(self.ftp_server)
            ftp.login(self.ftp_user, self.ftp_password)
            ftp.cwd(remotefileDir) 
            files = ftp.nlst()
            ftp.quit()
            r = re.compile(regEx)
            filtered_list = list(filter(r.match, files))
            filtered_list.sort()
            for file in filtered_list:
                if not os.path.isfile(os.path.join(basepath,file)):
                    self.log.logger.info("Download " + file)
                    self.downloadFile(remotefileDir+'/'+file,basepath,file+".tmp")
                    if fixtimeunits:
                        fixTimeUnits(os.path.join(basepath,file)+".tmp")
                    if file==filtered_list[-1] and nameforlastfile!="":
                        if os.path.isfile(os.path.join(basepath,nameforlastfile)):
                            os.remove(os.path.join(basepath,nameforlastfile))
                        os.rename(os.path.join(basepath,file)+".tmp",os.path.join(basepath,nameforlastfile))
                        shutil.copy(os.path.join(basepath,nameforlastfile),os.path.join(basepath,file))
                    os.rename(os.path.join(basepath,file)+".tmp",os.path.join(basepath,file))
                    
        except Exception as exp:
            self.log.logger.error(exp)
            return False
        
    def downloadFileIfNew(self,remotefile,basepath,localfile,keepOld=False,fixtimeunits=False):
        try:
            download=False            
            localfile_tmp=localfile+'.tmp'
            ftp = ftplib.FTP(self.ftp_server)
            ftp.login(self.ftp_user, self.ftp_password)
            remotefile_timestamp = ftp.voidcmd("MDTM "+remotefile)[4:].strip()
            ftp.quit()
            remotefile_timestamp = parser.parse(remotefile_timestamp)
            if os.path.isfile(os.path.join(basepath,localfile)):
                localfile_stat = os.stat(os.path.join(basepath,localfile))
                if (parser.parse(time.ctime(localfile_stat.st_mtime)).date()<(remotefile_timestamp.date())):
                    download=True
                elif (parser.parse(time.ctime(localfile_stat.st_mtime)).date()<datetime.today().date()) and (remotefile_timestamp.date() == datetime.today().date()):
                    download=True
            else:
                download=True           
            if download:
                self.log.logger.info("Download "+remotefile)
                self.downloadFile(remotefile,basepath,localfile_tmp)
                if keepOld and os.path.isfile(os.path.join(basepath,localfile)):                    
                    shutil.copy(os.path.join(basepath,localfile),os.path.join(basepath,localfile+parser.parse(time.ctime(localfile_stat.st_mtime)).date().strftime("%Y%m%d")))
                if fixtimeunits:
                    fixTimeUnits(os.path.join(basepath,localfile_tmp))                
                shutil.copy(os.path.join(basepath,localfile_tmp),os.path.join(basepath,localfile))
                os.remove(os.path.join(basepath,localfile_tmp))
        except Exception as exp:
            self.log.logger.error(exp)
            return False
        return download

    def downloadFileFiltrados(self,remoteFile,basepath,localfile):
        try:
            if (self.downloadFile(remoteFile,basepath,localfile)):
                with open(os.path.join(basepath,localfile), 'r') as file:
                    filedata = file.read()
                filedata = filedata.replace(',', '   ').replace('Nan', 'NaN')
                with open(os.path.join(basepath,localfile), 'w') as file:
                    file.write(filedata)
                df=pd.read_csv(os.path.join(basepath,localfile),sep="[ ^]+",engine='python')
                df['F'] = pd.to_datetime(df['F'], unit='s')
                days=df['F'].dt.strftime("%Y/%m/%d").unique().tolist()
                for day in days:
                    mask=df[(df.F.dt.day == int(day.split('/')[2])) & (df.F.dt.month == int(day.split('/')[1])) &(df.F.dt.year == int(day.split('/')[0])) ]
                    df_path=os.path.join(basepath,"FiltradosResumen_Resultados_Filt_"+day.replace("/","_")+".csv")
                    if os.path.isfile(df_path):
                        old_df=pd.read_csv(df_path)
                        old_df['F'] = pd.to_datetime(old_df['F'], format="%Y/%m/%d %H:%M:%S")
                        new_df=pd.concat([old_df,mask], axis=0,ignore_index=True)
                        new_df.drop_duplicates(subset=['F'],keep='first', inplace=True) 
                        new_df.reset_index(drop=True, inplace=True)
                    else:
                        new_df=mask
                    new_df.to_csv(df_path,index=False, float_format='%g')
                os.remove(os.path.join(basepath,localfile))
                return True
        except Exception as exp:
            self.log.logger.error(exp)
            return False

    def downloadFileAgitacion(self,remoteFile,basepath,localfile):
        try:
            self.downloadFile(remoteFile,basepath,localfile)
            df=pd.read_fwf(os.path.join(basepath,localfile))
            df.rename( columns={'Unnamed: 1':'Dato'}, inplace=True )
            df.rename( columns={'Tiempo,Dato':'time'}, inplace=True )
            df['time'] = pd.to_datetime(df['time'], unit='s')
            days=df['time'].dt.strftime("%Y/%m/%d").unique().tolist()
            for day in days:
                mask=df[(df.time.dt.day == int(day.split('/')[2])) & (df.time.dt.month == int(day.split('/')[1])) &(df.time.dt.year == int(day.split('/')[0])) ]
                df_path=os.path.join(basepath,"agitacion_"+day.replace("/","_")+".csv")
                if os.path.isfile(df_path):
                    old_df=pd.read_csv(df_path)
                    old_df['time'] = pd.to_datetime(old_df['time'], format="%Y/%m/%d %H:%M:%S")
                    new_df=pd.concat([old_df,mask], axis=0,ignore_index=True)
                    new_df.drop_duplicates(subset=['time'],keep='first', inplace=True) 
                    new_df.reset_index(drop=True, inplace=True)
                else:
                    new_df=mask
                new_df.to_csv(df_path,index=False, float_format='%g')
            os.remove(os.path.join(basepath,localfile))            
        except Exception as exp:
            self.log.logger.error(exp)
            return False
        