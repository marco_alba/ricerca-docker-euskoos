import logging
from logging.handlers import TimedRotatingFileHandler

class customlogger:
    #set logger
    def __init__(self, name, filepath,when='midnight', backupCount=7,level=logging.DEBUG):
        logFormatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        self.logger = logging.getLogger(name)    
        handler = TimedRotatingFileHandler(filepath, when='midnight', backupCount=7)
        handler.setFormatter(logFormatter)
        self.logger.addHandler(handler)    
        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(logFormatter)
        self.logger.addHandler(consoleHandler)    
        self.logger.setLevel(level)
