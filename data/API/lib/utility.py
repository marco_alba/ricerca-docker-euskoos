from netCDF4 import Dataset
from datetime import datetime, timedelta
from cftime import num2date, date2num

def fixTimeUnits(file):
    ds = Dataset(file, "a")
    dates = num2date(ds["time"][:],units=ds["time"].units)
    ds["time"].units="seconds since 1970-01-01T00:00:00Z"
    ds["time"][:]=date2num(dates,units=ds["time"].units)
    ds.close()    
  