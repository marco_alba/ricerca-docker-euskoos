import socket
import sys
import os

def get_lock(process_name,logger):
    if os.name == 'nt':
        return ""
    else:        
        get_lock._lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
        try:
            get_lock._lock_socket.bind('\0' + process_name)
            logger.info ('I got the lock')
            return ""
        except socket.error:
            logger.info ('lock exists')
            sys.exit()