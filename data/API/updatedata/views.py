###django import
from django.shortcuts import render
from rest_framework.decorators import api_view
from django.http import HttpResponse

###system import
from os.path import join
import pandas as pd
from os.path import join, isfile
from os import remove

###lib import
from lib.getlock import get_lock
from lib.logger import customlogger
from lib.downloadftp import customDownloader

###config import
from config.config import *

from drf_yasg.utils import swagger_auto_schema

@swagger_auto_schema(method='get',operation_description="Display the log of the selected datasetid (with no datasetid return the list of datasetids available)", responses={404: 'slug not found'})
@api_view(['GET'])
def log(request,datasetid=""):
    if datasetid=="":
        value = """Available logs:
        Bermeo_Tidal_Gauge_Agitation
        boyaDonostia_NRT_hourly_data        
        EuskalmetWAMBasqueCoast
        MetModelEuskalmetMetWinddata
        Pasaia_Tidal_Gauge_Agitation
        Pasaia_Tidal_Gauge_Filtrados
        MeteoFrance"""     
        return HttpResponse(value, content_type="text/plain")
    else:
        with open(log_path+datasetid+'.log','r') as f:
            file_content = f.read()    
        return HttpResponse(file_content, content_type="text/plain")

@api_view(['GET'])
def BermeoTidalGaugeAgitation(request):
    datasetid=BermeoTidalGaugeAgitation_datasetid
    basepath=BermeoTidalGaugeAgitation_basepath
    remoteFile=BermeoTidalGaugeAgitation_remoteFile
    localfile=BermeoTidalGaugeAgitation_localfile
    log=customlogger(datasetid,log_path+datasetid+'.log','midnight',10)
    get_lock(datasetid,log.logger)
    log.logger.info ('START')
    downloader=customDownloader(ftp_server,ftp_user,ftp_password,log)
    if (downloader.downloadFileAgitacion(remoteFile,basepath,localfile)):
        with open(join(flag_directory,datasetid), "a") as f:
            f.write("")
    log.logger.info ('END')
    return HttpResponse("DONE", content_type="text/plain")    

@api_view(['GET'])
def BermeoTidalGaugeFilt(request):
    remoteFile=BermeoTidalGaugeFilt_remoteFile 
    localfile=BermeoTidalGaugeFilt_localfile 
    basepath=BermeoTidalGaugeFilt_basepath 
    datasetid=BermeoTidalGaugeFilt_datasetid     
    log=customlogger(datasetid,log_path+datasetid+'.log','midnight',10)
    get_lock(datasetid,log.logger)
    log.logger.info ('START')
    downloader=customDownloader(ftp_server,ftp_user,ftp_password,log)
    if (downloader.downloadFileFiltrados(remoteFile,basepath,localfile)):
        with open(join(flag_directory,datasetid), "a") as f:
            f.write("")
        log.logger.info ('END')
        return HttpResponse("DONE", content_type="text/plain")    
    else:
        return HttpResponse("ERROR", content_type="text/plain")  

@api_view(['GET'])
def DonostiaBuoyNRThourlydata(request):
    remoteFile=DonostiaBuoyNRThourlydata_remoteFile 
    localfile=DonostiaBuoyNRThourlydata_localfile 
    basepath=DonostiaBuoyNRThourlydata_basepath 
    datasetid=DonostiaBuoyNRThourlydata_datasetid     
    log=customlogger(datasetid,log_path+datasetid+'.log','midnight',10)
    get_lock(datasetid,log.logger)
    log.logger.info ('START')
    downloader=customDownloader(ftp_server,ftp_user,ftp_password,log)
    if downloader.downloadFile(remoteFile,basepath,localfile):
        df=pd.read_fwf(join(basepath,localfile),skiprows=5,skipfooter=3)
        df.rename( columns={'Unnamed: 0':'time'}, inplace=True )
        df.drop([0, 1], inplace=True)
        mask = ~df['time'].str.contains("00:00")
        df.loc[mask, 'time'] = df.loc[mask, 'time'].str.strip()+" 00:00:00"
        df['time'] = pd.to_datetime(df['time'], format="%d/%m/%Y %H:%M:%S")
        df.to_csv(join(basepath,localfile+".tmp"),index=False)
        df=pd.read_csv(join(basepath,localfile+".tmp"))
        df['time'] = pd.to_datetime(df['time'], format="%Y/%m/%d %H:%M:%S")
        months=df['time'].dt.strftime("%m/%Y").unique().tolist()
        for month in months:
            mask=df[(df.time.dt.month == int(month.split('/')[0])) &(df.time.dt.year == int(month.split('/')[1])) ]
            df_path=join(basepath,localfile.replace(".txt","")+"_"+month.replace("/","_")+".csv")
            if isfile(df_path):
                old_df=pd.read_csv(df_path)
                old_df['time'] = pd.to_datetime(old_df['time'], format="%Y/%m/%d %H:%M:%S")
                new_df=pd.concat([old_df,mask], axis=0,ignore_index=True)
                new_df.drop_duplicates(subset=['time'],keep='first', inplace=True) 
                new_df.reset_index(drop=True, inplace=True)
            else:
                new_df=mask
            new_df.to_csv(df_path,index=False)
        remove(join(basepath,localfile+".tmp"))
        remove(join(basepath,localfile))
        with open(join(flag_directory,datasetid), "a") as f:
            f.write("")
        log.logger.info ('END')
    return HttpResponse("DONE", content_type="text/plain")    

@api_view(['GET'])
def EuskalmetWAMBasqueCoast(request):
    remoteFile=EuskalmetWAMBasqueCoast_remoteFile 
    localfile=EuskalmetWAMBasqueCoast_localfile 
    basepath=EuskalmetWAMBasqueCoast_basepath 
    datasetid=EuskalmetWAMBasqueCoast_datasetid 
    log=customlogger(datasetid,log_path+datasetid+'.log','midnight',10)
    get_lock(datasetid,log.logger)
    downloader=customDownloader(ftp_server,ftp_user,ftp_password,log)
    if (downloader.downloadFileIfNew(remoteFile,basepath,localfile)):
        with open(join(hardFlag_directory,datasetid), "a") as f:
            f.write("")
    return HttpResponse("DONE", content_type="text/plain")    

@api_view(['GET'])
def MetModelEuskalmetMetWinddata(request):
    remoteFile_d9=MetModelEuskalmetMetWinddata_remoteFile_d9 
    localfile_d9=MetModelEuskalmetMetWinddata_localfile_d9 
    remoteFile_d27=MetModelEuskalmetMetWinddata_remoteFile_d27 
    localfile_d27=MetModelEuskalmetMetWinddata_localfile_d27 
    remoteDir=MetModelEuskalmetMetWinddata_remoteDir 
    regEX=MetModelEuskalmetMetWinddata_regEX 
    basepath=MetModelEuskalmetMetWinddata_basepath 
    datasetids=MetModelEuskalmetMetWinddata_datasetids
    
    log=customlogger('MetModelEuskalmetMetWinddata',log_path+'MetModelEuskalmetMetWinddata.log','midnight',10)
    get_lock("MetModelEuskalmetMetWinddata",log.logger)
    downloader=customDownloader(ftp_server,ftp_user,ftp_password,log)

    if (downloader.downloadFileIfNew(remoteFile_d9,basepath,localfile_d9,True,True)):
        with open(join(hardFlag_directory,datasetids[0]), "a") as f:
            f.write("")

    if (downloader.downloadFileIfNew(remoteFile_d27,basepath,localfile_d27,False,True)):
        with open(join(hardFlag_directory,datasetids[1]), "a") as f:
            f.write("")

    if (downloader.downloadFiles(remoteDir,regEX,basepath,True,datasetids[2])):
        with open(join(flag_directory,datasetids[2]), "a") as f:
            f.write("")

    return HttpResponse("DONE", content_type="text/plain")    

@api_view(['GET'])
def PasaiaTidalGaugeAgitation(request):
    remoteFile=PasaiaTidalGaugeAgitation_remoteFile
    localfile=PasaiaTidalGaugeAgitation_localfile
    basepath=PasaiaTidalGaugeAgitation_basepath
    datasetid=PasaiaTidalGaugeAgitation_datasetid    
    log=customlogger(datasetid,log_path+datasetid+'.log','midnight',10)
    get_lock(datasetid,log.logger)
    log.logger.info ('START')
    downloader=customDownloader(ftp_server,ftp_user,ftp_password,log)
    if (downloader.downloadFileAgitacion(remoteFile,basepath,localfile)):
        with open(join(flag_directory,datasetid), "a") as f:
            f.write("")
    log.logger.info ('END')
    return HttpResponse("DONE", content_type="text/plain")    

@api_view(['GET'])
def PasaiaTidalGaugeFilt(request):
    remoteFile=PasaiaTidalGaugeFilt_remoteFile 
    localfile=PasaiaTidalGaugeFilt_localfile 
    basepath=PasaiaTidalGaugeFilt_basepath 
    datasetid=PasaiaTidalGaugeFilt_datasetid     
    log=customlogger(datasetid,log_path+datasetid+'.log','midnight',10)
    get_lock(datasetid,log.logger)
    log.logger.info ('START')
    downloader=customDownloader(ftp_server,ftp_user,ftp_password,log)
    if (downloader.downloadFileFiltrados(remoteFile,basepath,localfile)):
        with open(join(flag_directory,datasetid), "a") as f:
            f.write("")
    log.logger.info ('END')
    return HttpResponse("DONE", content_type="text/plain")    
