from django.urls import path
from . import views

urlpatterns = [
    path('BermeoTidalGaugeAgitation', views.BermeoTidalGaugeAgitation,name='BermeoTidalGaugeAgitation'),
    path('BermeoTidalGaugeFilt', views.BermeoTidalGaugeFilt,name='BermeoTidalGaugeFilt'),
    path('DonostiaBuoyNRThourlydata', views.DonostiaBuoyNRThourlydata,name='DonostiaBuoyNRThourlydata'),
    path('EuskalmetWAMBasqueCoast', views.EuskalmetWAMBasqueCoast,name='EuskalmetWAMBasqueCoast'),
    path('MetModelEuskalmetMetWinddata', views.MetModelEuskalmetMetWinddata,name='MetModelEuskalmetMetWinddata'),
    path('PasaiaTidalGaugeAgitation', views.PasaiaTidalGaugeAgitation,name='PasaiaTidalGaugeAgitation'),
    path('PasaiaTidalGaugeFilt', views.PasaiaTidalGaugeFilt,name='PasaiaTidalGaugeFilt'),
    path('log/', views.log,name='log'),
    path('log/<datasetid>', views.log,name='log'),
]

