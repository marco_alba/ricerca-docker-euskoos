# ---------------------------------------------------------------------------- #
# ---------------------------------------------------------------------------- #
# ----------------------- EUSKOOS Configuration File ------------------------- #
# ---------------------------------------------------------------------------- #
# ---------------------------------------------------------------------------- #

#LOG Configuration
#log_path="/home/usremodpace/scripts/log/"
log_path="ph_API_log_path" #'/data/log/'

#FTP configuration
ftp_server="ph_ftp_server" #"ftp2.azti.es"
ftp_user="ph_ftp_user" #"dmc"
ftp_password="ph_ftp_password" #"boyasWS"

#ERDDAP directory
hardFlag_directory='/erddapData/hardFlag/'
flag_directory='/erddapData/flag/'

#BermeoTidalGaugeAgitation
BermeoTidalGaugeAgitation_remoteFile="/mareografoBermeo/productos/agitacion.dat"
BermeoTidalGaugeAgitation_localfile="agitacion.dat"
BermeoTidalGaugeAgitation_basepath="/data/Bermeo_Tidal_Gauge/Agitacion/"
BermeoTidalGaugeAgitation_datasetid="Bermeo_Tidal_Gauge_Agitacion"

#BermeoTidalGaugeFilt
BermeoTidalGaugeFilt_remoteFile="/mareografoBermeo/raw/Filtrados/Resumen_Resultados_Filt.dat"
BermeoTidalGaugeFilt_localfile="Resumen_Resultados_Filt.dat"
BermeoTidalGaugeFilt_basepath="/data/Bermeo_Tidal_Gauge/Filtrados/"
BermeoTidalGaugeFilt_datasetid="Bermeo_Tidal_Gauge_Filt"

#DonostiaBuoyNRThourlydata
DonostiaBuoyNRThourlydata_remoteFile="boyaDonostia.txt"
DonostiaBuoyNRThourlydata_localfile="boyaDonostia.txt"
DonostiaBuoyNRThourlydata_basepath='/data/boyaDonostia_NRT_hourly_data/'
DonostiaBuoyNRThourlydata_datasetid="boyaDonostia_NRT_hourly_data"

#EuskalmetWAMBasqueCoast
EuskalmetWAMBasqueCoast_remoteFile="WAMEKMT.nc"
EuskalmetWAMBasqueCoast_localfile="WAMEKMT.nc"
EuskalmetWAMBasqueCoast_basepath="/data/EuskalmetWAMBasqueCoast/"
EuskalmetWAMBasqueCoast_datasetid="EuskalmetWAMBasqueCoast"

#MetModelEuskalmetMetWinddata
MetModelEuskalmetMetWinddata_remoteFile_d9="/modelo_meteo/d9.nc"
MetModelEuskalmetMetWinddata_localfile_d9="d9.nc"
MetModelEuskalmetMetWinddata_remoteFile_d27="/modelo_meteo/d27.nc"
MetModelEuskalmetMetWinddata_localfile_d27="d27.nc"
MetModelEuskalmetMetWinddata_remoteDir="modelo_meteo"
MetModelEuskalmetMetWinddata_regEX="[0-9]{8}\.nc"
MetModelEuskalmetMetWinddata_basepath="/data/MetModelEuskalmetMetWinddata/"
MetModelEuskalmetMetWinddata_datasetids=["Euskalmet_d9","Euskalmet_d27","Euskalmet_daily"]

#PasaiaTidalGaugeAgitation.py
PasaiaTidalGaugeAgitation_remoteFile="/mareografoPasaia/productos/agitacion.dat"
PasaiaTidalGaugeAgitation_localfile="agitacion.dat"
PasaiaTidalGaugeAgitation_basepath="/data/Pasaia_Tidal_Gauge/Agitacion/"
PasaiaTidalGaugeAgitation_datasetid="Pasaia_Tidal_Gauge_Agitacion"

#PasaiaTidalGaugeFilt
PasaiaTidalGaugeFilt_remoteFile="/mareografoPasaia/raw/Filtrados/Resumen_Resultados_Filt.dat"
PasaiaTidalGaugeFilt_localfile="Resumen_Resultados_Filt.dat"
PasaiaTidalGaugeFilt_basepath="/data/Pasaia_Tidal_Gauge/Filtrados/"
PasaiaTidalGaugeFilt_datasetid="Pasaia_Tidal_Gauge_Filtrados"