#!/bin/bash
set -e

# preferable to fire up Tomcat via start-tomcat.sh which will start Tomcat with
# security manager, but inheriting containers can also start Tomcat via
# catalina.sh

if [ "$1" = 'start-tomcat.sh' ] || [ "$1" = 'catalina.sh' ]; then
    ###
    # ETT Custom user
    groupadd --gid 2035 usremodpace && \
    useradd -u 2035 -g usremodpace -s /sbin/nologin usremodpace
    ###

    USER_ID=${TOMCAT_USER_ID:-1000}
    GROUP_ID=${TOMCAT_GROUP_ID:-1000}

    ###
    # Tomcat user
    ###
    groupadd -r tomcat -g ${GROUP_ID} && \
    useradd -u ${USER_ID} -g tomcat -d ${CATALINA_HOME} -s /sbin/nologin \
        -c "Tomcat user" tomcat
    # Set GECOS field for tomcat user
    #chfn --other='umask=0002' tomcat

    ###
    # Change CATALINA_HOME ownership to tomcat user and tomcat group
    # Restrict permissions on conf
    ###

    chown -R tomcat:tomcat ${CATALINA_HOME} && chmod 400 ${CATALINA_HOME}/conf/*
    sync

    if [ ! -z "$DEFAULT_PALETTE" ]; then
        sed -i "/<\/Context>/i <Parameter name=\"defaultPalette\" value=\"$DEFAULT_PALETTE\" override=\"true\"/>" $CATALINA_HOME/conf/Catalina/localhost/ncWMS.xml
    fi

    if [ ! -z "$ADVERTISED_PALETTES" ]; then
        sed -i "/<\/Context>/i <Parameter name=\"advertisedPalettes\" value=\"$ADVERTISED_PALETTES\" override=\"true\"/>" $CATALINA_HOME/conf/Catalina/localhost/ncWMS.xml
    fi

    ###
    # ETT Custom user
    usermod -a -G usremodpace tomcat
    ###

    ###
    # Set ncWMS admin password
    ###
    cd /usr/local/tomcat/bin/
    hash_pwd=$(./digest.sh -a SHA 'ncWMS_PASSWORD' | grep 'ncWMS_PASSWORD')
    hash_pwd=${hash_pwd#'ncWMS_PASSWORD:'}
    sed -i "s@<user username=\"admin\"@@" /usr/local/tomcat/conf/tomcat-users.xml
    sed -i "s@password=\"d033e22ae348aeb5660fc2140aec35850c4da997\"@@" /usr/local/tomcat/conf/tomcat-users.xml
    sed -i "s@roles=\"manager-gui,manager-script,manager-jmx,manager-status,admin-script,admin-gui\"/>@@" /usr/local/tomcat/conf/tomcat-users.xml
    sed -i "s@placeholder@${hash_pwd}@" /usr/local/tomcat/conf/tomcat-users.xml

    exec gosu tomcat "$@"
fi

exec "$@"
