#!/bin/bash
### Set script working path ###
DEPLOYROOTSCRIPT="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
### ###

#### DEBUG ####
echo The deploy root script dir is ${DEPLOYROOTSCRIPT}
read -p "Press ENTER key to start" ignoreusrinput
#### END DEBUG ####

# Import script variables
echo -n read variables...
source ${DEPLOYROOTSCRIPT}/docker-EnvCreationVariable.sh
echo DONE!

### Apache ###
# Import Apache setup
source ${DEPLOYROOTSCRIPT}/docker-ApacheSetup.sh
# Set default index.html
echo "This is a default index" > $APACHE_SITE_PATH/index.html
### ###

### Create linux users ###
echo -n create linux users...
useradd -m -u 2020 -s /sbin/nologin usrtomcat > /dev/null 2>&1
usermod -L usrtomcat > /dev/null 2>&1
usermod -c ",,,,umask=0002" usrtomcat > /dev/null 2>&1
usermod -a -G ${ERDDAP_user} usrtomcat > /dev/null 2>&1
echo DONE!
### ###


### Create folders for application docker environment ###
echo -n create folders for application docker environment...
# Moving in the root directory of the application docker environment
cd ${MYDOCKER_ROOT_DIR}
# Creating the directories tree
mkdir -p ${MYDOCKER_ROOT_DIR}/customdocker/{customvolumes,deployfiles}
mkdir -p ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/{erddapContent,erddapData,ncWMSconfig}
mkdir -p ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/environments
mkdir -p ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/ncWMS-docker/environments
mkdir -p ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/entrypoints
mkdir -p ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/ncWMS-docker/entrypoints
mkdir -p ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/API
# Set directories permissions
chmod -R 775 ${MYDOCKER_ROOT_DIR}/customdocker/
chmod -R g+s ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes
chmod -R g+s ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles

chown -R root:docker ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes
chown -R root:docker ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles
chown -R usrtomcat:usrtomcat ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent
chown -R usrtomcat:usrtomcat ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapData
chown -R usrtomcat:usrtomcat ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/ncWMSconfig
# Only fot RedHat-like distribution - Set direcotries context
semanage fcontext -a -t container_file_t "${MYDOCKER_ROOT_DIR}/customdocker/customvolumes(/.*)?"
semanage fcontext -a -t container_share_t "${MYDOCKER_ROOT_DIR}/customdocker/deployfiles(/.*)?"
restorecon -RF ${MYDOCKER_ROOT_DIR}/customdocker

echo DONE!
### ###

### Move files folders in the created directories ###
cp -R ${DEPLOYROOTSCRIPT}/data/erddap ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker
cp -R ${DEPLOYROOTSCRIPT}/data/ncWMS ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/ncWMS-docker
cp -R ${DEPLOYROOTSCRIPT}/data/API/* ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/API/
cp -R ${DEPLOYROOTSCRIPT}/entrypoints/erddap_entrypoint.sh ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/entrypoints/erddap_entrypoint.sh
cp -R ${DEPLOYROOTSCRIPT}/entrypoints/ncWMS_entrypoint.sh ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/ncWMS-docker/entrypoints/ncWMS_entrypoint.sh
cp -R ${DEPLOYROOTSCRIPT}/environments/erddap-compose.env ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/environments/erddap-compose.env
cp -R ${DEPLOYROOTSCRIPT}/environments/ncWMS-compose.env ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/ncWMS-docker/environments/ncWMS-compose.env
### ###

###Set values in erddap_entrypoint.sh script
sed -i "s@ERDDAP_user@${ERDDAP_user}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/entrypoints/erddap_entrypoint.sh
sed -i "s@ph_createtunnel@${createtunnel}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/entrypoints/erddap_entrypoint.sh
if [ "$createtunnel" == "YES" ]
then
    sed -i "s@ph_sshpass@${sshpass}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/entrypoints/erddap_entrypoint.sh
    sed -i "s@ph_sshserver@${sshserver}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/entrypoints/erddap_entrypoint.sh
    sed -i "s@ph_sshport@${sshport}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/entrypoints/erddap_entrypoint.sh
fi

###Set ncWMS_PASSWORD value in ncWMS_entrypoint.sh script
sed -i "s@ncWMS_PASSWORD@${ncWMS_PASSWORD}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/ncWMS-docker/entrypoints/ncWMS_entrypoint.sh

### Load container default content
echo -n load container default content...
## ERDDAP ##
# Extract default ERDDAP Content folder (orgin: https://coastwatch.pfeg.noaa.gov/erddap/download/setup.html#initialSetup - https://github.com/BobSimons/erddap/releases/download/v2.12/erddapContent.zip)
tar xzf ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/erddap/content.erddap_v214.tar.gz -C ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent
rm -f ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/datasets.xml
cp ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/erddap/datasets.xml ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/datasets.xml
chown -R usrtomcat:usrtomcat ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/*

###Set values in datasets.xml
if [ "$createtunnel" == "NO" ]
then
    sed -i "s@127.0.0.1:3306@${mysqlserver}:${mysqlport}@g" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/datasets.xml
fi

# For ERDDAP version < 2.12
# Set ERDDAP docker environment variable
sed -i "s@<bigParentDirectory>/home/yourName/erddap/</bigParentDirectory>@<bigParentDirectory>${ERDDAP_bigParentDirectory}</bigParentDirectory>@g" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<baseUrl>http://localhost:8080</baseUrl>@<baseUrl>${ERDDAP_baseUrl}</baseUrl>@g" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<baseHttpsUrl></baseHttpsUrl>@<baseHttpsUrl>${ERDDAP_baseHttpsUrl}</baseHttpsUrl>@g" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s|<emailEverythingTo>your.email@yourInstitution.edu</emailEverythingTo>|<emailEverythingTo>${ERDDAP_emailEverythingTo}</emailEverythingTo>|g" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<adminInstitution>Your Institution</adminInstitution>@<adminInstitution>${ERDDAP_adminInstitution}</adminInstitution>@" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<adminInstitutionUrl>Your Institution's or Group's Url</adminInstitutionUrl>@<adminInstitutionUrl>${ERDDAP_adminInstitutionUrl}</adminInstitutionUrl>@" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<adminIndividualName>Your Name</adminIndividualName>@<adminIndividualName>${ERDDAP_adminIndividualName}</adminIndividualName>@" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<adminPosition>ERDDAP administrator</adminPosition>@<adminPosition>${ERDDAP_adminPosition}</adminPosition>@" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<adminPhone>+1 999-999-9999</adminPhone>@<adminPhone>${ERDDAP_adminPhone}</adminPhone>@" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<adminAddress>123 Main St.</adminAddress>@<adminAddress>${ERDDAP_adminAddress}</adminAddress>@" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<adminCity>Some Town</adminCity>@<adminCity>${ERDDAP_adminCity}</adminCity>@" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<adminStateOrProvince>CA</adminStateOrProvince>@<adminStateOrProvince>${ERDDAP_adminStateOrProvince}</adminStateOrProvince>@" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<adminPostalCode>99999</adminPostalCode>@<adminPostalCode>${ERDDAP_adminPostalCode}</adminPostalCode>@" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<adminCountry>USA</adminCountry>@<adminCountry>${ERDDAP_adminCountry}</adminCountry>@" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s|<adminEmail>your.email@yourCompany.com</adminEmail>|<adminEmail>${ERDDAP_adminemail}</adminEmail>|" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<flagKeyKey>CHANGE THIS TO YOUR FAVORITE QUOTE</flagKeyKey>@<flagKeyKey>${ERDDAP_flagKeyKey}</flagKeyKey>@" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml
sed -i "s@<variablesMustHaveIoosCategory>true</variablesMustHaveIoosCategory>@<variablesMustHaveIoosCategory>false</variablesMustHaveIoosCategory>@" ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/erddapContent/setup.xml

echo "ERDDAP_MIN_MEMORY=\"${ERDDAP_MIN_MEMORY}\"" >> ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/environments/erddap-compose.env
echo "ERDDAP_MAX_MEMORY=\"${ERDDAP_MAX_MEMORY}\"" >> ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/environments/erddap-compose.env
# For ERDDAP version >= 2.14
# Set ERDDAP docker environment variable
echo "# ERDDAP VARIABLES" >> ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/environments/erddap-compose.env
echo "ERDDAP_bigParentDirectory=\"${ERDDAP_bigParentDirectory}\"" >> ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/environments/erddap-compose.env
echo "ERDDAP_baseUrl=\"${ERDDAP_baseUrl}\"" >> ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/environments/erddap-compose.env
echo "ERDDAP_baseHttpsUrl=\"${ERDDAP_baseHttpsUrl}\"" >> ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/environments/erddap-compose.env
echo "ERDDAP_emailEverythingTo=\"${ERDDAP_emailEverythingTo}\"" >> ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/erddap-docker/environments/erddap-compose.env
## ##
### ###

## ncWMS ##
cp -R ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/ncWMS-docker/ncWMS/ncWMS2.xml  ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/ncWMSconfig/ncWMS2.xml
cp -R ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/ncWMS-docker/ncWMS/config.xml  ${MYDOCKER_ROOT_DIR}/customdocker/customvolumes/ncWMSconfig/config.xml
                                                                                      
## API ##
mkdir -p "${MYDOCKER_DATA_DIR}/Bermeo_Tidal_Gauge/Agitacion/"
mkdir -p "${MYDOCKER_DATA_DIR}/Bermeo_Tidal_Gauge/Filtrados/"
mkdir -p "${MYDOCKER_DATA_DIR}/boyaDonostia_NRT_hourly_data/"
mkdir -p "${MYDOCKER_DATA_DIR}/EuskalmetWAMBasqueCoast/"
mkdir -p "${MYDOCKER_DATA_DIR}/MetModelEuskalmetMetWinddata/"
mkdir -p "${MYDOCKER_DATA_DIR}/Pasaia_Tidal_Gauge/Agitacion/"
mkdir -p "${MYDOCKER_DATA_DIR}/Pasaia_Tidal_Gauge/Filtrados/"
mkdir -p "${API_log_path}/"

chown -R usrtomcat:usrtomcat ${MYDOCKER_DATA_DIR}/Bermeo_Tidal_Gauge
chown -R usrtomcat:usrtomcat ${MYDOCKER_DATA_DIR}/boyaDonostia_NRT_hourly_data
chown -R usrtomcat:usrtomcat ${MYDOCKER_DATA_DIR}/EuskalmetWAMBasqueCoast
chown -R usrtomcat:usrtomcat ${MYDOCKER_DATA_DIR}/MetModelEuskalmetMetWinddata
chown -R usrtomcat:usrtomcat ${MYDOCKER_DATA_DIR}/Pasaia_Tidal_Gauge
chown -R usrtomcat:usrtomcat ${API_log_path}

sed -i "s@ALLOWED_HOSTS = \[\]@ALLOWED_HOSTS = \[\'${APACHE_DOMAIN_ROOT}\'\]@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/API/EUSKOOSAPI/settings.py
sed -i "s@DEBUG = True@DEBUG = False@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/API/EUSKOOSAPI/settings.py
sed -i "s@ph_API_log_path@${API_log_path}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/API/config/config.py
sed -i "s@ph_ftp_server@${ftp_server}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/API/config/config.py
sed -i "s@ph_ftp_user@${ftp_user}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/API/config/config.py
sed -i "s@ph_ftp_password@${ftp_password}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/API/config/config.py
if [ "${APACHE_ENABLE_SSL}" == "YES" ]
then
    sed -i "s@ph_apiurl@https://${APACHE_DOMAIN_ROOT}/${APACHE_PROXY_LOCATION_API}/updatedata/@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/API/euskoos.sh
else
    sed -i "s@ph_apiurl@http://${APACHE_DOMAIN_ROOT}/${APACHE_PROXY_LOCATION_API}/updatedata/@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/API/euskoos.sh
fi
sed -i "s@ph_MYDOCKER_ROOT_DIR@${MYDOCKER_ROOT_DIR}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/API/euskoos_cron

### Set docker-compose file ###
cp ${DEPLOYROOTSCRIPT}/templates/docker-compose.yaml.template ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/docker-compose.yaml
sed -i "s@ph_HOST_ERDDAP_PORT@${MYDOCKER_HOST_ERDDAP_PORT}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/docker-compose.yaml
sed -i "s@ph_MYDOCKER_ROOT_DIR@${MYDOCKER_ROOT_DIR}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/docker-compose.yaml
sed -i "s@ph_MYDOCKER_DATA_DIR@${MYDOCKER_DATA_DIR}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/docker-compose.yaml
sed -i "s@ph_HOST_ncWMS_PORT@${MYDOCKER_HOST_ncWMS_PORT}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/docker-compose.yaml
sed -i "s@ph_HOST_API_PORT@${MYDOCKER_HOST_API_PORT}@g" ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/docker-compose.yaml
### ###
echo DONE!

echo "build docker image"
cd  ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/
docker-compose up --no-start
echo DONE!

read -r -p "Do you wish to start ERDDAP? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
    docker-compose up -d erddap
fi
read -r -p "Do you wish to start ncWMS? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
    docker-compose up -d ncWMS
fi
read -r -p "Do you wish to start API? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
    docker-compose up -d api
fi
read -r -p "Do you wish to set the default cron jobs for the API (the /etc/crontab file will be modified)? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
then
    echo "${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/API/euskoos_cron" >> /etc/crontab
else
    echo "this are the suggested cron expression to use for updating the datasets:"
    cat ${MYDOCKER_ROOT_DIR}/customdocker/deployfiles/API/euskoos_cron
fi



